package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable, KeyListener {

	public static final int width = 500;
	public static final int height = 300;
	
    //Render
	private Graphics2D g2d;
	private BufferedImage image;
		
	//Game Loop
	private Thread thread;
	private boolean running;
	private long targetTime;
	
	//Game Stuff
	private int SIZE = 10;
	private Entity ball;
	private Entity head;								//snake
	private Entity headPaddle, tailPaddle;				//paddle
	private Entity wallhead, topcorner, bottomcorner;	//wall
	private DoubleLinkedList<Entity> snake,paddle,wall;
	private boolean gameover, point;
    private int xball = 1, yball = 0;
    private int object = 0;									// Snake = 1, Paddle = 2, Wall = 3, 
    private Entity clash;
    
    //key input
	private boolean up,down,right,left,start;
	private boolean paddleup = true, paddledown;
		
	//movement
	private int dx,dy;
	
	public GamePanel()
	{
		setPreferredSize(new Dimension(width, height));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}
	
	@Override
	public void addNotify() {
		super.addNotify();
		thread = new Thread(this);
		thread.start();
	}
	
	private void setFPS(int fps)
	{
		targetTime = 1000 / fps;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int k = e.getKeyCode();
		
		if(k == KeyEvent.VK_UP) up = true;
		if(k == KeyEvent.VK_DOWN) down = true;
		if(k == KeyEvent.VK_LEFT) left = true;
		if(k == KeyEvent.VK_RIGHT) right = true;
		if(k == KeyEvent.VK_ENTER) start = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int k = e.getKeyCode();
		
		if(k == KeyEvent.VK_UP) up = false;
		if(k == KeyEvent.VK_DOWN) down = false;
		if(k == KeyEvent.VK_LEFT) left = false;
		if(k == KeyEvent.VK_RIGHT) right = false;
		if(k == KeyEvent.VK_ENTER) start = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void run() {
		if(running) return;
		init();
		long startTime;
		long elapsed;
		long wait;
		while(running)
		{
			startTime = System.nanoTime();
			
			update();
			requestRender();
			
			
			elapsed = System.nanoTime() - startTime;
			wait = targetTime - elapsed / 1000000;
			if(wait > 0)
			{
				try
				{
					Thread.sleep(wait);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	private void init()
	{
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		g2d = image.createGraphics();
		running = true;
		setUplevel();
		setFPS(10);
	}
	
	private void setUplevel()
	{
		
		//Setup Snake
		head = new Entity(SIZE);
		head.isSnake();
	    snake = new DoubleLinkedList<Entity>(head);
		head.setPosition(3*width/4, height/2);
		for(int i = 1; i < 5; i++)
		{ 
			Entity e = new Entity(SIZE);
			e.setPosition(head.getX() + (i*SIZE), head.getY());
			snake.append(e);
			e.isSnake();
		}
		
		//Setup paddle
		headPaddle = new Entity(SIZE);
		paddle = new DoubleLinkedList<Entity>(headPaddle);
		headPaddle.setPosition(50, height/2);
		for(int i = 1; i < 5; i++)
		{
			Entity e = new Entity(SIZE);
			e.setPosition(headPaddle.getX(), headPaddle.getY()+(i*SIZE));
			paddle.append(e);
			tailPaddle = e;
		}
		
		//Setup wall
		wallhead = new Entity(SIZE);
		wallhead.setPosition(0, 0);
		wall = new DoubleLinkedList<Entity>(wallhead);
		for(int i = 1; i < width/10; i++) {
			Entity e = new Entity(SIZE);
			e.setPosition(wallhead.getX()+(i*SIZE), wallhead.getY());
			wall.append(e);
			topcorner = e;	
		}
		for(int i = 1; i < height/10; i++) {
			Entity e = new Entity(SIZE);
			e.setPosition(topcorner.getX(), topcorner.getY()+(i*SIZE));
			wall.append(e);
			bottomcorner = e;			
		}
		for(int i = 1; i < width/10; i++) {
			Entity e = new Entity(SIZE);
			e.setPosition(bottomcorner.getX()-(i*SIZE), bottomcorner.getY());
			wall.append(e);	
		}
		
		//Setup ball
		setupBall();
				
		//extra
		point = false;
		gameover = false;
		dx = dy = 0;
	}
	
	private void setupBall()
	{
		clash = head;
		yball = 0;
		xball = 1;
		ball = new Entity(SIZE);
		ball.isSnake();
		ball.setPosition(width/2 , height/2);
	}
	
	private void requestRender() {
		render(g2d);
		Graphics g = getGraphics();
		g.drawImage(image,0,0,null);
		g.dispose();
	}
	
	private void update() {
		if(gameover)
		{
			
			if(start)
			{
				setUplevel();
			}
			return;
		}
		
		if (point)
		{
			setupBall();
		}

		//Snake movement
		if(up && dy == 0)
		{
			dy = -SIZE;
			dx = 0;
		}
		if(down && dy == 0)
		{
			dy = SIZE;
			dx = 0;
		}
		if(left && dx == 0)
		{
			dy = 0;
			dx = -SIZE;
		}
		if(right && dx == 0)
		{
			dy = 0;
			dx = SIZE;
		}
		
		if(dx != 0 || dy != 0)
		{
			Entity e = new Entity(SIZE);
			e.setPosition(head.getX() + dx, head.getY() + dy);
			if (testCollision(e) || head.getX() < 0 ) gameover = true;
			else
			{
				snake.prepend(e);
				head = e;
				e.isSnake();
				if (!point) snake.remove(snake.getTail());
			}
			
		}
		
		//paddle movement
		/*int p = paddle.getNextElement(paddle.getNextElement(headPaddle)).getY();
		if((p - ball.getY() > 20 || p - ball.getY() < -20))
		{
		if (p > ball.getY() )
		{
			paddleup = true;
			paddledown = false;
		}
		else 
		{
			paddledown = true;
			paddleup = false;
		}*/
		Entity g = new Entity(SIZE);
		int y = 0;
		if (paddleup) {
			y = headPaddle.getY();
			if (y <= 10)
			{
				paddleup = false;
				paddledown = true;
			}
			else {
				g.setPosition(headPaddle.getX(), y - 10);
				if (testCollision(g)) gameover = true;
				paddle.prepend(g);
				paddle.remove(paddle.getTail());
				headPaddle = g;
				tailPaddle = paddle.getTail();
			} 				
		}
		
		if(paddledown) {
			y = tailPaddle.getY();
			if (y >= height-20)
			{
				paddledown= false;
				paddleup = true;
			}
			else {
				g.setPosition(tailPaddle.getX(), y + 10);
				if (testCollision(g)) gameover = true;
				paddle.append(g);
				paddle.remove(paddle.getHead());
				tailPaddle = g;
				headPaddle = paddle.getHead();
			}
		//}
		}
		
		//ball movement
	    if(testCollision(ball))
			{
				if(yball == 0) yball = 1;
				if(object == 1)
				{
					if(ball.getX() < clash.getX()) xball = -1;
					else xball = 1;
					if(ball.getY() < clash.getY()) yball = -1;
					else yball = 1;
					System.out.println(ball.getX() + "	" + clash.getX());
					
				}
				if(object == 2) xball = -xball;
				if(object == 4) {
					xball = -xball;
					yball = -yball;
				}
				if(object == 3) {
					if (ball.getX() >= 480) xball = -xball;
					else yball = -yball;
				}
				if(clash == topcorner || clash == bottomcorner)
				{
					xball = -1;
					yball = -yball;
				}
			}
		ball.setPosition(ball.getX()+xball*5, ball.getY()+yball*5);
		if (ball.getX() < 0) point = true;
		else point = false;
		
		
	}
	
	public boolean testCollision(Entity e)
	{
		boolean collision = false;
		
		//Collision with Snake
		Entity s = head;
		for(int i = 0 ; i < snake.getSize() - 1 ; i++)
		{
			if(s.isCollision(e)) 
			{
				clash = s;
				collision = true;
				object = 1;
				break;
			}
			s = snake.getNextElement(s);
		}
		
		if (snake.getTail().isCollision(e))
		{
			clash = snake.getTail();
			collision =  true;
			object = 1;
		}
		
		//Collision with wall
		Entity w = wallhead;
		for(int i = 0 ; i < wall.getSize() - 1 ; i++)
		{
			if(w.isCollision(e)) 
			{
				
				clash = w;
				object = 3;
				collision = true;
				break;
			}
			w = wall.getNextElement(w);
		}
		if(wall.getTail().isCollision(e)) 
			{
				clash = wall.getTail();
				object = 3;
				collision =  true;
			}
		
		//Collision with paddle
		Entity p = headPaddle;
		for(int i = 0 ; i < paddle.getSize() - 1 ; i++)
		{
			if(p.isCollision(e)) 
			{
				if (p == headPaddle) object = 4;
				object = 2;
				clash = p;
				collision = true;
				break;
			}
			p = paddle.getNextElement(p);
		}
		if(tailPaddle.isCollision(e))
		{
			clash = tailPaddle;
			object = 4;
			collision =  true;
		}
		
		return collision;
	}
	
	public void render(Graphics2D g2d)
	{
		g2d.clearRect(0, 0, width, height);
		g2d.setColor(Color.YELLOW);
		Entity s = head;
		for(int i = 0 ; i < snake.getSize() - 1 ; i++)
		{
			s.render(g2d);
			Entity temp = snake.getNextElement(s);
			s = temp;
		}
		s.render(g2d);
		
		//Paddle
		g2d.setColor(Color.BLUE);
		Entity p = headPaddle;
		for(int i = 0 ; i < paddle.getSize() - 1 ; i++)
		{
			p.render(g2d);
			Entity temp = paddle.getNextElement(p);
			p = temp;
		}
		p.render(g2d);
		
		//ball
		g2d.setColor(Color.GREEN);
		ball.render(g2d);
		
		//Wall
		g2d.setColor(Color.WHITE);
		Entity w = wallhead;
		for(int i = 0 ; i < wall.getSize() - 1 ; i++)
		{
			w.render(g2d);
			Entity temp = wall.getNextElement(w);
			w = temp;
		}
		w.render(g2d);
		
		if (point)
		{
			g2d.drawString("SCORE +1", 400, 50);
		}
		
		if (gameover)
		{
			g2d.drawString("GAMEOVER !", 250, 50);
			g2d.drawString("PRESS ENTER", 250, 70);
			g2d.setColor(Color.RED);
			head.render(g2d);
		}

		if(dx == 0 && dy == 0)
		{
			g2d.drawString("READY FREDDY?", 250, 50);
		}
	}

}
