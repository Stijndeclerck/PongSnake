package game;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

class TestDoubleLinkedList {

DoubleLinkedList<String> list; 
	
	@BeforeEach
	void setUp(){
		list = new DoubleLinkedList<String>("begin");
		list.append("einde");
	}
 
	@Test
	void testGetSize() {
		DoubleLinkedList<String> dll = new DoubleLinkedList<String>();
		assertEquals(0, dll.getSize());
		assertEquals(2, list.getSize());
	}

	@Test
	void testPrepend() {
		list.prepend("ervoor");
		assertEquals("ervoor" , list.getHead());
		assertEquals(3 ,list.getSize());

	}
 
	@Test
	void testAppend() {
		list.append("erachter");
		assertEquals("erachter" , list.getTail());
		assertEquals(3 , list.getSize());
	}

	@Test
	void testGetHead() {
		assertEquals("begin" , list.getHead());
	}

	@Test
	void testGetTail() {
		assertEquals("einde" , list.getTail());
	}
 
	@Test
	void testRemove() {
		list.remove("begin");
		assertEquals(1, list.getSize());
		assertEquals("einde", list.getHead());
		assertEquals("einde", list.getTail());
		list.remove("einde");
		assertEquals(0, list.getSize());

	}

	@Test
	void testGetNextElement() {
		assertEquals("einde", list.getNextElement("begin") );
	}
	
}
