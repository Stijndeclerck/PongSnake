package game;

public class DoubleLinkedList<E> {

		private int size;
		private Node head;
		private Node tail;
		
		public DoubleLinkedList() {
			this(null);
			size = 0;
		}
		 
		public DoubleLinkedList(E element) {
			size = 1;
			Node n = new Node(element);
			head = n;
			tail = n;
		}
				
		public int getSize() {
			return size;
		}
		
		
		public void prepend(E element) {
			Node temp = new Node(element, head, null);
			head.setPrev(temp);
			head = temp;
			size++;
		}
		
		public void append(E element) {
			Node temp = new Node(element, null, tail);
			tail.setNext(temp);
			tail = temp;
			size++;
		}
		
		public E getHead() {
			return head.getElement();
		}
		
		public E getTail() {
			return tail.getElement();
		}
		
		public void remove(E element) {
			Node toBeRemoved = null;
			Node test = head;
			if(head.getElement().equals(element)) {
				toBeRemoved = head;
				if (test.getNext() == null) {
					head = null;
				    tail = null;
				}
				else {
				head = toBeRemoved.getNext();
				head.setPrev(null);
				}
				
			}
			else if (tail.getElement().equals(element)) {
				toBeRemoved = tail;
				tail = toBeRemoved.getPrev();
				tail.setNext(null);
				
			}
			else {
				for(int i = 0; i < size-1; i++)
				{
					test = test.getNext();
					if (test.equals(element))
					{
						toBeRemoved = test;
						toBeRemoved.getPrev().setNext(toBeRemoved.getNext());
						toBeRemoved.getNext().setPrev(toBeRemoved.getPrev());
						break;
					}
				}
	 			 
			}
			toBeRemoved.setNext(null);
			toBeRemoved.setPrev(null);
			size--;
		}
		
		public E getNextElement(E element)
		{
			Node curs = head;
				for( ;curs.getElement() != element; )
				{
					Node temp = curs.getNext();
					curs = temp;
				}
				E next = curs.getNext().getElement();
				if(next == null) return null;
				else return next;
		}
				
		private class Node {
			private E element;
			private Node next;
			private Node prev;
			
			
			public Node(E element) {
				this(element, null, null);
			}
			
			public Node(E element, Node next, Node prev) {
				this.element = element;
				this.next = next;
				this.prev = prev;
			}
			
			
			public E getElement() {
				return element;
			}
			
			public Node getNext(){
				return next;
			}
		 	
			public Node getPrev(){
				return prev;
			}
			
			public void setNext(Node next) {
				this.next = next;
			}
			
			public void setPrev(Node prev) {
				this.prev = prev;
			}
			
			
		}
			
}

